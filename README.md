# Energy Australia Quiz

The quiz is to build a frontend app that consumes a given [API](http://eacodingtest.digital.energyaustralia.com.au/api-docs/#/cars/APICarsGet) and display the car shows in the frontend grouped by car make and model instead of the original API response. For full quiz description, refer to [here](http://eacodingtest.digital.energyaustralia.com.au/)

## Server

Due to a problem (400 - Downstream Error) in the provided [eaAPI](http://eacodingtest.digital.energyaustralia.com.au/api/v1/cars), I've created a backend NodeJS service that does the same functionality following the [swagger specs here](http://eacodingtest.digital.energyaustralia.com.au/api-docs/#/cars/APICarsGet) to continue the frontend development.

- Go to `/server` and install the dependencies by running `npm i`
- Run the server using `npm start`

## Client

- This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 7.3.3. 
- Go to `/client` and install the project dependencies by running `npm i`

### Development server

Run `npm start` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

**Test data**

For testing, these are sample data populated from the backend in `/server/models/db.js`

```
{
  carShow: [
    {
      name: "Show 1",
      cars: [
        {
          make: "Make Motors 1",
          model: "Model 1A"
        },
        {
          make: "Make Motors 1",
          model: "Model 1B"
        },
        {
          make: "Make Motors 3",
          model: "Model 3A"
        },
        {
          make: "Make Motors 3",
          model: "Model 3B"
        },
        {
          make: "Make Motors 3",
          model: "Model 3C"
        },
        {
          make: "Make Motors 7",
          model: "Model 7A"
        }
      ]
    },
    {
      name: "Show 2",
      cars: [
        {
          make: "Make Motors 1",
          model: "Model 1A"
        },
        {
          make: "Make Motors 1",
          model: "Model 1B"
        },
        {
          make: "Make Motors 3",
          model: "Model 3A"
        },
        {
          make: "Make Motors 4",
          model: "Model 4A"
        },
        {
          make: "Make Motors 4",
          model: "Model 4B"
        },
        {
          make: "Make Motors 5",
          model: "Model 5A"
        },
        {
          make: "Make Motors 5",
          model: "Model 5B"
        },
        {
          make: "Make Motors 5",
          model: "Model 5C"
        }
      ]
    }
  ]
```

### Running end-to-end tests

- Make sure both the server and client are running.
- Run `npm run e2e` to execute the end-to-end tests via Cypress.

### Running unit tests

- Run `npm test` to execute the unit tests via [Karma](https://karma-runner.github.io).
