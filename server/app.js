const express = require('express')
const bodyParser = require('body-parser');
const cors = require('cors')
const app = express()
const port = 3000

app.use(cors())
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

require('./controllers/car.controller')(app)

app.listen(port, () => console.log(`Server is listening on port ${port}!`))
