const CarShow = require('../models/carshow.model')

const findAll = (req, res) => {
    return CarShow.findAll()
        .then(data => res.json(data))
        .catch(err => res.json({ error: { code: 'CARSHOWS_NOT_FOUND', message: err } }))
}

module.exports = app => {
    app.get('/api/v1/cars', findAll)
}
