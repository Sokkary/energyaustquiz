module.exports = {
  carShow: [
    {
      name: "Show 1",
      cars: [
        {
          make: "Make Motors 1",
          model: "Model 1A"
        },
        {
          make: "Make Motors 1",
          model: "Model 1B"
        },
        {
          make: "Make Motors 3",
          model: "Model 3A"
        },
        {
          make: "Make Motors 3",
          model: "Model 3B"
        },
        {
          make: "Make Motors 3",
          model: "Model 3C"
        },
        {
          make: "Make Motors 7",
          model: "Model 7A"
        }
      ]
    },
    {
      name: "Show 2",
      cars: [
        {
          make: "Make Motors 1",
          model: "Model 1A"
        },
        {
          make: "Make Motors 1",
          model: "Model 1B"
        },
        {
          make: "Make Motors 3",
          model: "Model 3A"
        },
        {
          make: "Make Motors 4",
          model: "Model 4A"
        },
        {
          make: "Make Motors 4",
          model: "Model 4B"
        },
        {
          make: "Make Motors 5",
          model: "Model 5A"
        },
        {
          make: "Make Motors 5",
          model: "Model 5B"
        },
        {
          make: "Make Motors 5",
          model: "Model 5C"
        }
      ]
    }
  ]
};
