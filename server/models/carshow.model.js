const carShow = require('./db').carShow;

const findAll = () => {    
    return Promise.resolve(carShow)
}

module.exports = {
    findAll,
}
