/// <reference types="Cypress" />

context('Actions', () => {

  it('navigate to the main page', () => {
    cy.visit('http://localhost:4200')
    cy.get('.title').contains("Car Shows")
  })

  it('results - should retrieve 5 car makes', () => {
    cy.get('.make').should('have.length', 5)
  })

  it('results - should retrieve 11 car models', () => {
    cy.get('.model').should('have.length', 11)
  })

  it('results - should retrieve 14 car shows', () => {
    cy.get('.show').should('have.length', 14)
  })

})
