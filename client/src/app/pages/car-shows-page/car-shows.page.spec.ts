import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { CarShowsPage } from './car-shows.page';
import { CarShowsListComponent } from 'src/app/components/car-shows-list/car-shows-list.component';

describe('CarShowsPage', () => {
  let component: CarShowsPage;
  let fixture: ComponentFixture<CarShowsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CarShowsPage, CarShowsListComponent ],
      imports: [FormsModule, HttpClientModule]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CarShowsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
