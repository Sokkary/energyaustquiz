import { Component, OnInit } from "@angular/core";
import { CarService } from "../../services/car.service";
import { Observable } from "rxjs";
import { reduce, tap } from "rxjs/operators";

@Component({
  selector: "app-car-shows-page",
  templateUrl: "./car-shows.page.html",
  styleUrls: ["./car-shows.page.css"]
})
export class CarShowsPage implements OnInit {
  carShowsGroupedByMakeModel$: Observable<any>;

  constructor(private carService: CarService) {}

  ngOnInit() {
    this.carShowsGroupedByMakeModel$ = this.carService.findAllShowsGroupedByCarMakeModel();
  }

  onSelectCarShow() {}
}
