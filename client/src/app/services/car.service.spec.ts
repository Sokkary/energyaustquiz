import { TestBed } from '@angular/core/testing';

import { CarService } from './car.service';
import { HttpClientModule } from '@angular/common/http';

describe('CarService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [HttpClientModule]
  }));

  it('should be created', () => {
    const service: CarService = TestBed.get(CarService);
    expect(service).toBeTruthy();
  });
});
