import { Injectable } from "@angular/core";
import {
  HttpClient,
  HttpHeaders,
  HttpErrorResponse
} from "@angular/common/http";
import { Observable, of } from "rxjs";
import { map, catchError, tap, reduce, publishLast, refCount } from "rxjs/operators";
import { environment } from "../../environments/environment";

@Injectable({
  providedIn: "root"
})
export class CarService {
  constructor(private http: HttpClient) {}

  findAllShows(): Observable<CarShow[]> {
    return this.http.get<CarShow[]>(`${environment.carsAPIBaseURL}/cars`).pipe(
      map(res => res || []),
      publishLast(),
      refCount(),
      catchError(this.handleError<any>("findAllShows"))
    );
  }

  findAllShowsGroupedByCarMakeModel(): Observable<any> {
    return this.findAllShows().pipe(
      reduce((cars, shows: CarShow[]) => {
        shows.forEach(show =>
          show.cars.forEach(car => {
            if (!cars[car.make]) {
              cars[car.make] = {};
            }

            if (!cars[car.make][car.model]) {
              cars[car.make][car.model] = [];
            }

            cars[car.make][car.model].push(show.name);
          })
        );
        return cars;
      }, {}),
      publishLast(),
      refCount()
    );
  }

  private handleError<T>(operation = "operation", result?: T) {
    return (error: any): Observable<T> => {
      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      console.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }
}
