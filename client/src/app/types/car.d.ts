interface Car {
    make: string;
    model: string;
}

interface CarShow {
    name: string;
    cars: Car[];
}