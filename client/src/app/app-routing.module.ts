import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CarShowsPage } from './pages/car-shows-page/car-shows.page';

const routes: Routes = [
  { path: '', component: CarShowsPage },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
