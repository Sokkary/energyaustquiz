import { Component, Input, Output, EventEmitter } from "@angular/core";
import { Observable } from "rxjs";

@Component({
  selector: "app-car-shows-list",
  templateUrl: "./car-shows-list.component.html",
  styleUrls: ["./car-shows-list.component.css"]
})
export class CarShowsListComponent {
  @Input()
  carShowsGroupedByMakeModel$: Observable<any>;

  @Output()
  select = new EventEmitter<CarShow>();

  constructor() {}

  selectCarShow(carShow) {
    this.select.emit(carShow);
  }
}
