import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CarShowsListComponent } from './car-shows-list.component';

describe('CarShowsListComponent', () => {
  let component: CarShowsListComponent;
  let fixture: ComponentFixture<CarShowsListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CarShowsListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CarShowsListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
