import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CarShowsListComponent } from './components/car-shows-list/car-shows-list.component';
import { CarShowsPage } from './pages/car-shows-page/car-shows.page';

@NgModule({
  declarations: [
    AppComponent,
    CarShowsListComponent,
    CarShowsPage,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
